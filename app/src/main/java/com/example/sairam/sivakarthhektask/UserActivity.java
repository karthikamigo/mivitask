package com.example.sairam.sivakarthhektask;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;


public class UserActivity extends AppCompatActivity {

    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.phone)
    TextView phone;
    @BindView(R.id.dob)
    TextView dob;
    @BindView(R.id.email)
    TextView email;
    @BindView(R.id.prepaid)
    TextView prepaid;
    @BindView(R.id.services)
    TextView service;
    @BindView(R.id.msn)
    TextView msn;
    @BindView(R.id.credits)
    TextView credit;
    @BindView(R.id.credit_expiry)
    TextView c_expiry;
    @BindView(R.id.subscription)
    TextView subscription;
    @BindView(R.id.included_data_bal)
    TextView include_bal;
    @BindView(R.id.auto_renual)
    TextView auto_renual;
    @BindView(R.id.primary_sub)
    TextView primary_subscription;

    @BindView(R.id.product)
    TextView product;
    @BindView(R.id.price)
    TextView price;
    @BindView(R.id.plan_name)
    TextView plan;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_data);
        ButterKnife.bind(this);
        try {
            JSONObject obj = new JSONObject(Util.AssetJSONFile("sample.json",UserActivity.this));
            JSONObject d_obj = obj.getJSONObject("data");
            JSONArray jArray = obj.getJSONArray("included");
            JSONObject a_obj = d_obj.getJSONObject("attributes");
            name.setText("Name : "+a_obj.getString("first-name")+" "+a_obj.getString("last-name"));
            phone.setText("Phone : "+a_obj.getString("contact-number"));
            dob.setText("Dob : "+a_obj.getString("date-of-birth"));
            email.setText("Email : "+a_obj.getString("email-address"));
            prepaid.setText("Payment : "+a_obj.getString("payment-type"));

            JSONObject ser_obj = jArray.getJSONObject(0);
            service.setText(ser_obj.getString("type"));
            JSONObject sa_obj = ser_obj.getJSONObject("attributes");
            msn.setText("Msn : "+sa_obj.getString("msn"));
            credit.setText("Credit : "+sa_obj.getString("credit"));
            c_expiry.setText("Credit Expiry : "+sa_obj.getString("credit-expiry"));
             JSONObject sub_obj = jArray.getJSONObject(1);
            subscription.setText(sub_obj.getString("type"));
            include_bal.setText("Data balance : "+sub_obj.getJSONObject("attributes").getString("included-data-balance"));
            auto_renual.setText("Auto renual  : "+String.valueOf(sub_obj.getJSONObject("attributes").getBoolean("auto-renewal")));
            primary_subscription.setText("Primary subscription : "+String.valueOf(sub_obj.getJSONObject("attributes").getBoolean("primary-subscription")));

            JSONObject product_obj = jArray.getJSONObject(2);
            product.setText(product_obj.getString("type"));
            plan.setText("Plan : "+product_obj.getJSONObject("attributes").getString("name"));
            price.setText("Price : "+product_obj.getJSONObject("attributes").getString("price"));



        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(UserActivity.this,LoginActivity.class));
    }
}
