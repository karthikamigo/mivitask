package com.example.sairam.sivakarthhektask;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

/**
 * Created by karthik on 6/22/2018.
 */
public class SplashActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_splash);

			Log.d("TAG","@@@ IN ELSE  Build.VERSION.SDK_INT >= 23");
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
                  startActivity(new Intent(SplashActivity.this,UserActivity.class));
				}
			}, 3000);

	}


}
